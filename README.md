# Derecho de autor cero

Aquí se encuentran todos los archivos empleados para desarrollar los *ebooks*
del artículo «Derecho de autor cero: entre el derecho de autor y el dominio público», 
editado por [Mariel Quirino Andrade](https://www.linkedin.com/in/mariel-quirino-007a86bb) y 
[publicado en el *blog*](http://marianaeguaras.com/derecho-de-autor-cero/) de
Mariana Eguaras.

<hr />

# Índice

* [Disponibilidad](#disponibilidad)
* [Árbol de directorios](#Árbol-de-directorios)
* [Contactos](#contactos)
  * [Ramiro Santa Ana Anguiano](#ramiro-santa-ana-anguiano)
  * [Mariana Eguaras](#mariana-eguaras)
* [Limitación de responsabilidad](#limitación-de-responsabilidad)
* [Licencia](#licencia)

<hr />

# Disponibilidad

La publicación tiene las siguientes opciones de descarga y visualización:

* HTML: [clic para visualizar](https://nikazhenya.github.io/derecho-de-autor-cero/).
* EPUB: [clic para descargar](https://github.com/NikaZhenya/derecho-de-autor-cero/raw/master/epub/derecho-de-autor-cero.epub).
* PDF: [clic para descargar](https://github.com/NikaZhenya/derecho-de-autor-cero/raw/master/tex/derecho-de-autor-cero.pdf).
* MOBI: [clic para descargar](https://github.com/NikaZhenya/derecho-de-autor-cero/raw/master/epub/derecho-de-autor-cero.mobi).

# Árbol de directorios

* `docs`. Todo lo relacionado a la versión HTML.
* `epub`. Todo lo relacionado a la versión EPUB y MOBI.
* `img`. La imagen de portada.
* `md`. El archivo madre de esta publicación.
* `tex`. Todo lo relacionado a la versión PDF
* `README.md`. ¡Este archivo que estás leyendo!

# Contactos

## Ramiro Santa Ana Anguiano

[xxx.cliteratu.re](http://xxx.cliteratu.re/)	

[ramiro.santaana@perrotriste.io](mailto:ramiro.santaana@perrotriste.io)

## Mariel Quirino Andrade

[Linkedin](https://www.linkedin.com/in/mariel-quirino-007a86bb)

[leiram.oniriuq@gmail.com](mailto:leiram.oniriuq@gmail.com)


## Mariana Eguaras

[www.marianaeguaras.com](http://marianaeguaras.com/)

[hola@marianaeguaras.com](mailto:hola@marianaeguaras.com)

# Limitación de responsabilidad

**Primera edición, 2017.**

***Última revisión: 22 de mayo del 2017.***

# Licencia

Todos los archivos están bajo [Licencia Editorial Abierta y Libre (LEAL)](http://www.leal.perrotriste.io).
